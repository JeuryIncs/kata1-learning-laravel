<?php


use Illuminate\Support\ServiceProvider;

class StorageServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind(
            'LibroRepositorio',
            'EloquentLibroRepositorio'
        );
    }

}