<div class="form-crear-libro container">
    {{ Form::open(['route' => 'home.create', 'role' => 'form', 'method' => 'put', 'files' => true]) }}
    <br>
    {{Form::label('nombre', 'Nombre');}}
    <br>
    {{ Form::text('title', null , array('class' => 'form-control form-nombre')) }}
    <br>
    {{ Form::label('Foto', 'Foto:', array('class' => 'form-foto')) }}
    {{ Form::input('file', null, null, array('class' => 'form-control form-upload')) }}
    <br/>
    {{ Form::label('Categoria', 'Categoria')}}
    <div class="form-group {{ $errors->has('id_autor') ? 'has-error' : ''  }}">
        {{ Form::select('id_director', $categorias->lists('nombre', 'id') , $categorias, array('class' => 'form-control form-autor', 'placeholder' => 'Director')) }}
        {{ $errors->first('id_director', '<span class="errors text-danger">:message</span>') }}
    </div>
    {{ Form::label('Autor', 'Autor', array('class' => 'form-create-autor')) }}
    <div class="form-group {{ $errors->has('id_autor') ? 'has-error' : ''  }}">
        {{ Form::select('id_director', $autor->lists('nombre', 'id') , $autor, array('class' => 'form-control form-categoria', 'placeholder' => 'Director')) }}
        {{ $errors->first('id_director', '<span class="errors text-danger">:message</span>') }}
    </div>
    {{Form::submit('Guardar', array('class' => 'btn btn-primary pull-right btn-guardar-libro'))}}
    {{ Form::close() }}
</div>