@extends('layout.master')

@section('content')
    <div class="row block">
        <div class="heading"><h2 class="text-center">Libros</h2></div>

        <button type="button" class="btn btn-primary pull-right btn-crear-libros">Crear Libro</button>

    </div>

    @include('layout.partials.crear')
    <div class="card-libro">

    </div>
@stop