<html>
<head>
    <title></title>
    {{HTML::Style("/css/zerogrid.css")}}
    {{HTML::Style("/css/bootstrap.css")}}
    {{HTML::Style("/css/style.css")}}
    {{HTML::Style("/css/responsive.css")}}

</head>
    <body>
    @include('layout.partials.header')
        <div class="container">
            @yield('content')
        </div>
        @include('layout.partials.footer')

        {{ HTML::script('/js/jquery-1.11.1.min.js') }}
        {{ HTML::script('/js/html5.js') }}
        {{ HTML::script('/js/app/script.js') }}
        {{ HTML::script('/js/css3-mediaqueries.js') }}
        {{ HTML::script('/js/bootstrap.min.js') }}
    </body>
</html>