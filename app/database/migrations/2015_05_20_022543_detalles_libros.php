<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesLibros extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('detalles_libros', function(Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger("id_libros");
            $table->unsignedInteger("id_categorias");
            $table->unsignedInteger("id_autor");
            $table->string("foto", 220);

            $table->foreign('id_autor')
                ->references('id')
                ->on('autor');

            $table->foreign('id_categorias')
                ->references('id')
                ->on('categorias');

            $table->foreign('id_libros')
                ->references('id')
                ->on('libros');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schemma::drop("detalles_libros");
	}

}
