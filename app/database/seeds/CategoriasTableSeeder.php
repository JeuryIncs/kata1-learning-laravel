<?php
/**
 * Created by PhpStorm.
 * User: gede2926
 * Date: 5/20/2015
 * Time: 10:12 AM
 */

class CategoriasTableSeeder extends \Illuminate\Database\Seeder {

    public function run()
    {
        Eloquent::unguard();

        Categories::create(['nombre' => 'Thriller']);
        Categories::create(['nombre' => 'Erotica']);
        Categories::create(['nombre' => 'Romantica']);
        Categories::create(['nombre' => 'Ficcion']);
        Categories::create(['nombre' => 'Emocinal']);
        Categories::create(['nombre' => 'Medicina']);
        Categories::create(['nombre' => 'Comedia']);
        Categories::create(['nombre' => 'Religiosa']);
    }

}