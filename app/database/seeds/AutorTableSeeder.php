<?php
/**
 * Created by PhpStorm.
 * User: gede2926
 * Date: 5/20/2015
 * Time: 9:56 AM
 */

class AutorTableSeeder extends \Illuminate\Database\Seeder{

    public function run()
    {
        Eloquent::unguard();

        Autors::create(['nombre' => 'Jeury Inc']);
        Autors::create(['nombre' => 'El Faraon']);
        Autors::create(['nombre' => 'El Billy']);
        Autors::create(['nombre' => 'Amon Dex']);
    }

}