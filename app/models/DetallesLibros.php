<?php
/**
 * Created by PhpStorm.
 * User: gede2926
 * Date: 5/20/2015
 * Time: 10:01 AM
 */

class DetallesLibros  extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'detalles_libros';

    /**
     * Autor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function autor()
    {
        return $this->hasMany('Autor', 'id_autor');
    }

    /**
     * Categorias
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categorias()
    {
        return $this->hasMany('Categories', 'id_categorias');
    }

    /**
     * Lirbos
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function libros()
    {
        return $this->hasMany('Libros', 'id_libros');
    }
}
