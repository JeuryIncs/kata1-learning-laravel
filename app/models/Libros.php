<?php
namespace models;
/**
 * Created by PhpStorm.
 * User: gede2926
 * Date: 5/20/2015
 * Time: 10:06 AM
 */

class Libros extends Eloquent  {

    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'libros';

    /**
     * Libros
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function libros()
    {
        return $this->hasMany('DetallesLibros', 'id_libros');
    }

    public function autors()
    {
        return $this->belongsTo('Autors', 'id_autor');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function genres()
    {
        return $this
            ->belongsToMany('Categories', 'id_categorias');
    }

}
