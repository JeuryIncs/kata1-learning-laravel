<?php
/**
 * Created by PhpStorm.
 * User: gede2926
 * Date: 5/20/2015
 * Time: 9:58 AM
 */

class Autors extends Eloquent  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'autor';

    /**
     * DetallesLibros
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detallesLibros()
    {
        return $this->hasMany('DetallesLibros', 'id_autor');
    }

}
