<?php

class Categories extends Eloquent {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categorias';

    /**
     * Categorias
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Categories()
    {
        return $this->hasMany('DetallesLibros', 'id_categorias');
    }

}