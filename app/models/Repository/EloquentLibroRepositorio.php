<?php
/**
 * Created by PhpStorm.
 * User: gede2926
 * Date: 5/20/2015
 * Time: 12:01 PM
 */
 use models\Libros;

 class EloquentLibroRepositorio implements LibroRepositorio {

     /**
      * @return mixed
      */
     public function getAll()
     {
         return Libros::all();
     }

     /**
      * @param $id
      * @return mixed
      */
     public function find($id)
     {

        return Libros::find($id);
     }

     /**
      * @param $input
      * @return mixed
      */
     public function create($input)
     {
         return Libros::create($input);
     }

     /**
      * @param $input
      * @return mixed
      */
     public function update($input)
     {
         return Libros::update($input);
     }

     /**
      * @param $id
      * @return mixed
      */
     public function delete($id)
     {

         Libros::detele($id);
     }
 }