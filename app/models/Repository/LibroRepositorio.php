
<?php

interface LibroRepositorio {

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * @param $input
     * @return mixed
     */
    public function create($input);

    /**
     * @param $input
     * @return mixed
     */
    public function update($input);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}