<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    /**
     * Repository
     * @var \LibroRepositorio
     */
    protected $repository;

    public function __construct(LibroRepositorio $repository)
    {
        $this->repository = $repository;
    }

	public function index()
	{
        $categorias = Categories::all();
        $autor = Autors::all();

        return View::make('layout.partials.index', compact('categorias', 'autor'));
	}

    public function create()
    {
        $input = Input::except('image');


        $libro = new \models\Libros($input);



    }

}
